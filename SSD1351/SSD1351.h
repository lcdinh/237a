#ifndef SSD1351_h
#define SSD1351_h

#include "mbed.h"
#include "BurstSPI.h"

//Pinouts for OLED SPI interface
#define OLED_MOSI  p5
#define OLED_SCLK  p7
#define OLED_CS    p8
#define OLED_DC    p9
#define OLED_RST   p10

//SSD1351 Regs
#define SSD1351_CMD_SETCOLUMN       0x15
#define SSD1351_CMD_SETROW          0x75
#define SSD1351_CMD_WRITERAM        0x5C
#define SSD1351_CMD_READRAM         0x5D
#define SSD1351_CMD_SETREMAP        0xA0
#define SSD1351_CMD_STARTLINE       0xA1
#define SSD1351_CMD_DISPLAYOFFSET   0xA2
#define SSD1351_CMD_DISPLAYALLOFF   0xA4
#define SSD1351_CMD_DISPLAYALLON    0xA5
#define SSD1351_CMD_NORMALDISPLAY   0xA6
#define SSD1351_CMD_INVERTDISPLAY   0xA7
#define SSD1351_CMD_FUNCTIONSELECT  0xAB
#define SSD1351_CMD_DISPLAYOFF      0xAE
#define SSD1351_CMD_DISPLAYON       0xAF
#define SSD1351_CMD_PRECHARGE       0xB1
#define SSD1351_CMD_DISPLAYENHANCE  0xB2
#define SSD1351_CMD_CLOCKDIV        0xB3
#define SSD1351_CMD_SETVSL          0xB4
#define SSD1351_CMD_SETGPIO         0xB5
#define SSD1351_CMD_PRECHARGE2      0xB6
#define SSD1351_CMD_SETGRAY         0xB8
#define SSD1351_CMD_USELUT          0xB9
#define SSD1351_CMD_PRECHARGELEVEL  0xBB
#define SSD1351_CMD_VCOMH           0xBE
#define SSD1351_CMD_CONTRASTABC     0xC1
#define SSD1351_CMD_CONTRASTMASTER  0xC7
#define SSD1351_CMD_MUXRATIO        0xCA
#define SSD1351_CMD_COMMANDLOCK     0xFD
#define SSD1351_CMD_HORIZSCROLL     0x96
#define SSD1351_CMD_STOPSCROLL      0x9E
#define SSD1351_CMD_STARTSCROLL     0x9F

/** SSD1351 Library for STM32F401RE Nucleo or STMstation P.1 development boards - may work with
 *  other targets, but not tested yet.
 *  
 *  Standard mbed SPI library is VERY slow, limiting frame rate. Using EricWieser's BurstSPI (which
 *  fixes compilation errors on the STM32F4XX improves throughput significantly.
 *  
 *  Tested on SSD1351 P/N UG-2828GDEDF11. May work with other SSD1351 panels but this is untested.
 *
 *  Example:
 *  @code
 *  
 *  #include "mbed.h"
 *  #include "SSD1351.h"
 *
 *  uint8_t buffer[128*128*2];
 *
 *  SSD1351 oled;
 *  //SSD1351 oled(PA_7,PA_5,PA_6,PC_5,PA_4);
 *  
 *
 *  int main(){
 *      oled.enableWrite();
 *      oled.setBuf(buffer);
 *      oled.fillBuf(0x0000);
 *      oled.printText("Hello World!",0,0,0x07E0,1)
 *      oled.writeBuf();
 *  }
 *
 *  @endcode
 */
class SSD1351{
    public:
        /** Connect to an SSD1351 on specified pins
         *  Connect to: (MOSI, SCLK)  <-- Native SPI pins
         *              (DC, CS, RST) <-- Any digital pins
         */
        SSD1351(PinName mosi_pin, PinName sclk_pin, PinName dc_pin, PinName cs_pin, PinName rst_pin);
        /** Connect to SSD1351 on STMstation P.1, or default pins specified in defines in SSD1351.h
         *  Default pins are:   MOSI    PA_7
         *                      SCLK    PA_5
         *                      CS      PA_6
         *                      DC      PC_5
         *                      RST     PA_4
         */
        SSD1351();
               
        //Rectangle fill without buffer
        //void fillRect(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint16_t fillcolor);
        
        /** Enable writing directly to the VRAM. This must be called at least once before calling 
         *  writeBuf().
         */
        void enableWrite();
        /** Fill the buffer with a single color
         *  @param fillcolor Unsigned 16-bit 565 RGB
         */
        void fillBuf(uint16_t fillcolor);
        /** Write the buffer to the VRAM. Make sure you call enableWrte() before doing this!
         */
        void writeBuf();
        /** Draw a sprite from flash memory, into the buffer
         *  @param s[]  Sprite containing unsigned 16-bit 565 RGB values (1D vector)
         *  @param x    x-coordinate of sprite
         *  @param y    y-coordinate of sprite
         *  @param w    Width of sprite
         *  @param h    Height of sprite
         *  @param mask This value in the sprite is "skipped" - transparancy value
         */
        void drawSpritePtr(const uint16_t s[] ,int16_t x, int16_t y, uint16_t w, uint16_t h, uint16_t mask);
        /** Fill the collision mask with a single value
         *  @param state Fill value
         */
        void fillCMask(uint8_t state);
        /** Draw a sprite from flash memory, into the collision map
         *  @param s[]  Sprite containing unsigned 16-bit 565 RGB values (1D vector)
         *  @param x     x-coordinate of sprite
         *  @param y     y-coordinate of sprite
         *  @param w     Width of sprite
         *  @param h     Height of sprite
         *  @param mask  This value in the sprite is "skipped" - transparancy value
         *  @param state Value written to the collision map
         */
        void drawCMask(const uint16_t s[], int16_t x, int16_t y, uint16_t w, uint16_t h, uint16_t mask, uint8_t state);
        /** Check if a sprite (not yet written to collision map) is going to collide with anything
         *  @param s[]  Sprite containing unsigned 16-bit 565 RGB values (1D vector)
         *  @param x    x-coordinate of sprite
         *  @param y    y-coordinate of sprite
         *  @param w    Width of sprite
         *  @param h    Height of sprite
         *  @param mask This value in the sprite is "skipped" - transparancy value
         */
        uint8_t checkCollision(const uint16_t s[], int16_t x, int16_t y, uint16_t w, uint16_t h, uint16_t mask);
        /** Draw a single character
         *  @param c     ASCII character 
         *  @param x     x-coordinate of character   
         *  @param y     y-coordinate of character   
         *  @param color Unsigned 16-bit 565 RGB   
         *  @param zoom  Scaling factor   
         */
        void drawChar(char c, int16_t x, int16_t y, uint16_t color, uint8_t zoom);
        /** Draw a single character
         *  @param c     Char array
         *  @param x     x-coordinate of character   
         *  @param y     y-coordinate of character   
         *  @param color Unsigned 16-bit 565 RGB   
         *  @param zoom  Scaling factor   
         */
        void printText(const char c[], int16_t x, int16_t y, uint16_t color, uint8_t zoom);
        /** Set the display buffer.
         *  @param _buf Buffer, must be uint8_t name[32768]
         */
        void setBuf(uint8_t* _buf);
        /** Set the collision map.
         *  @param _cmask Collision map, must be uint8_t name[16384]
         */
        void setCMask(uint8_t* _cmask);
        
        //Drawing primitives
        
        /** Draw a filled rectangle to buffer
         *  @param x     x-coordinate
         *  @param y     y-coordinate
         *  @param w     Width
         *  @param h     Height
         *  @param color Unsigned 16-bit 565 RGB
         */
        void fillRect(int16_t x, int16_t y, uint16_t w, uint16_t h, uint16_t color);
        /** Draw a open rectangle to buffer
         *  @param x     x-coordinate
         *  @param y     y-coordinate
         *  @param w     Width
         *  @param h     Height
         *  @param color Unsigned 16-bit 565 RGB
         */
        void openRect(int16_t x, int16_t y, uint16_t w, uint16_t h, uint16_t color);
        /** Draw a horizontal line without position calculations
         *  @param x      x-coordinate
         *  @param y      y-coordinate
         *  @param length Length, can be positive or negative
         *  @param color  Unsigned 16-bit 565 RGB
         */
        void drawHLine(int16_t x, int16_t y, int16_t length, uint16_t color);
        /** Draw a vertical line without position calculations
         *  @param x      x-coordinate
         *  @param y      y-coordinate
         *  @param length Length, can be positive or negative
         *  @param color  Unsigned 16-bit 565 RGB
         */
        void drawVLine(int16_t x, int16_t y, int16_t length, uint16_t color);
        /** Draw a line using Bresenham algorithm
         *  @param x1     Start x-coordinate
         *  @param y1     Start y-coordinate
         *  @param x2     End x-coordinate
         *  @param y2     End y-coordinate
         *  @param color  Unsigned 16-bit 565 RGB
         */
        void drawLine(int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t color);
        /** Draw open circle
         *  @param x0     Center x-coordinate
         *  @param y0     Center y-coordinate
         *  @param radius Circle radius
         *  @param color  Unsigned 16-bit 565 RGB
         */
        void openCircle(int16_t x0, int16_t y0, uint16_t radius, uint16_t color);
        /** Draw filled circle
         *  @param x0     Center x-coordinate
         *  @param y0     Center y-coordinate
         *  @param radius Circle radius
         *  @param color  Unsigned 16-bit 565 RGB
         */
        void fillCircle(int16_t x0, int16_t y0, uint16_t radius, uint16_t color);
        
    private:
        void begin();
        void spiwrite(uint8_t c);
        void writeCommand(uint8_t c);
        void writeData(uint8_t c);
        DigitalOut cs, dc, rst;
        BurstSPI spi;
        uint8_t *buf, *collisionmask;
};

#endif